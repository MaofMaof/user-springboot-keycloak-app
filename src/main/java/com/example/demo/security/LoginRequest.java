package com.example.demo.security;

import lombok.Value;

@Value
public class LoginRequest {
    String username;
    String password;
}
