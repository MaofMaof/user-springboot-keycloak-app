package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminResponseToken {
    String access_token;
    String refresh_token;
    int expires_in;
    int refresh_expires_in;
}
